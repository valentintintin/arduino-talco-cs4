// Programmation frequence pour NJ8822

#define PLL_LOCKED 5
#define DATA 6
#define CLK 7
#define CS 8
#define LED 9
#define TX 10

#define KHZ 100.0
#define MHZ 100000.0

unsigned long freqOsc = 21.4 * MHZ;
unsigned int stepFreq = 6.4 * KHZ;
unsigned int P = 64; // SP8718

unsigned long freq = 145.650 * MHZ;
unsigned int R;
unsigned int M;
unsigned int A;
unsigned int N;

bool tx = false;

void setup() {
  pinMode(PLL_LOCKED, INPUT);
  pinMode(DATA, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(CS, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(TX, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(TX, LOW);

  Serial.begin(115200);
  Serial.println(F("Start !"));

  delay(500);
  
  doPll();
}

void loop() {
  while (Serial.available() > 0) {
    char command = Serial.read();
    String data = Serial.readString();

    Serial.print(F("Command = ")); Serial.print(command); Serial.print(F("   Data = ")); Serial.println(data);
    
    if (command == 'F') {
      freq = data.toFloat() * MHZ;
    } else if (command == 'O') {
      freqOsc = data.toFloat() * MHZ;
    } else if (command == 'S') {
      stepFreq = data.toFloat() * KHZ;
    } else if (command == 'P') {
      P = data.toInt();
    } else if (command == 'T') {
      tx = (bool) data.toInt();
    }
    
    doPll();
  }

  if (tx && isLocked()) {
    Serial.println(F("TX"));
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(TX, HIGH);
    delay(500);
    Serial.println(F("RX"));
    digitalWrite(TX, LOW);
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
  }
  
  delay(10);
}

void doPll() {
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(LED, LOW);
  digitalWrite(TX, LOW);
  
  // http://radiomods.free.fr/f6csx/er2001/NJ88C22.pdf
  
  R = freqOsc / (2 * stepFreq);
  N = freq / stepFreq;
  M = int(N / P);
  A = N % P;  
  bool isOk = true;
  
  Serial.print(F("FreqOsc = ")); Serial.print(freqOsc / MHZ); Serial.print(F("   stepFreq = "));  Serial.print(stepFreq / KHZ); Serial.print(F("   Divider (P) = "));  Serial.println(P);
  Serial.print(F("Freq = ")); Serial.print(freq / MHZ); Serial.print(F("  R = ")); Serial.print(R); Serial.print(F("  N = ")); Serial.print(N); Serial.print(F("  M = ")); Serial.print(M); Serial.print(F("  A = ")); Serial.println(A);

  if (R < 6 || R > 4094) {
    isOk = false;
    Serial.println(F("R is outside range [6; 4094]"));
  }

  if (M < 8 || M > 1023) {
    isOk = false;
    Serial.println(F("M is outside range [8; 1023]"));
  }

  if (A > 127) {
    isOk = false;
    Serial.println(F("A is outside range [0; 127]"));
  }

  if (!isOk) {
    return;
  }

  /* DATA : DATA is high for a ‘1’ and low for a ‘0’. There are three data words which control the NJ88C22;
MSB is first in the order: ‘A’ (7 bits), ‘M’ (10 bits), ‘R’ (11 bits). */

  /* CLOCK : Data is clocked on the negative transition of the CLOCK waveform. If less than 28 negative clock
  transitions have been received when the ENABLE line goes low (i.e., only ‘M’ and ‘A’ will have been
  clocked in), then the ‘R’ counter latch will remain unchanged and only ‘M’ and ‘A’ will be transferred from
  the input shift register to the counter latches. This will protect the ‘R’ counter from being corrupted by
  any glitches on the clock line after only ‘M’ and ‘A’ have been loaded If 28 negative transitions have
  been counted, then the ‘R’ counter will be loaded with the new data.*/

  /* ENABLE : When ENABLE is low, the DATA and CLOCK inputs are disabled internally. As soon as ENABLE is
high, the DATA and CLOCK inputs are enabled and data may be clocked into the device. The data is
transferred from the input shift register to the counter latches on the negative transition of the ENABLE
input and both inputs to the phase detector are synchronised to each other. */
  
  digitalWrite(CLK, HIGH);
  wait();
  digitalWrite(CS, HIGH);

  doData(A, 7);
  doData(M, 10);
  doData(R, 11);
  
  wait();
  digitalWrite(CS, LOW);
  wait();
  digitalWrite(CLK, LOW);

  isLocked();
  
  digitalWrite(LED_BUILTIN, LOW);
}

void doData(unsigned int data, byte length) {
  //Serial.print(F("Do ")); Serial.print(data); Serial.print(F(" Length ")); Serial.println(length);
  
  unsigned int mask = 1 << length;
  for (byte i = length; i > 0; i--) {
    digitalWrite(DATA, data & mask);
    wait();
    digitalWrite(CLK, HIGH);
    wait();
    digitalWrite(CLK, LOW);
    wait();
    digitalWrite(DATA, LOW);

    if (i > 1) {
      wait();
    }

    //Serial.print(i); Serial.print(F(" ")); Serial.print(mask); Serial.print(F(" ")); Serial.println((data & mask) > 0);
    mask >>= 1;
  } 
}

void wait() {
  delayMicroseconds(10);
}

bool isLocked() {
  delay(1000);
  
  unsigned int pllLocked = digitalRead(PLL_LOCKED);
  bool result = pllLocked == 0;

  Serial.print(F("PLL locked = ")); Serial.println(result);

  digitalWrite(LED, result);

  return result;
}
