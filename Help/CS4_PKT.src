;
;===================================================
; Programme de pilotage module HF radiot�l�phone CS4
;===================================================
;
;
;
; Fr�quences pour utilisation PACKET
;
;
; origine F5SOH (trx 430 packet)
;
; modifi� par F6HCC:
;	- 144 MHz
;	- changement imm�diat de la fr�quence en r�ception (pour roue codeuse)
;	- oscillateur � quartz (pour �viter les rayonnements glissants)
;
;
; Utilisant un pic16f84, pas de 12.5Khz avec oscillateur de r�f�rence 6.4Mhz.
;
; Possibilit� d'utiliser un pic16f627 ou 16f628 => voir 3 lignes � modifier
; et revoir la configuration lors de la programmation du circuit
;
;
; Fref = 12.5KHz (fr�quence de boucle)
; diviseur par 64 'SP8718'
; fi � 21,4 MHz, battement supradyne.
;
; 'R' sur 11 bits = Fosc /(2 x Fcomp) = 6400/(2x12.5) = 512
; Changement de fr�quence avec 'M' (10bits) et 'A'(7bits)
; oscillateur du pic pilot� par quartz 2 � 4 MHz
;
; 16 canaux avec s�lection via 4 cavaliers ou roue codeuse
;
; Le passage en �mission est d�pendant du verrouillage du Pll pour �viter les splatters. (cmd_em)
;

                DEVICE  PIC16F84,XT_OSC,WDT_OFF,PWRT_OFF,PROTECT_OFF

                id     '6HCC'

                org     0Ch	;16f84 debut de la ram
		;org     20h	;pour 16F627 / 16F628 debut de la ram


Count0          DS      1
Count1          DS      1
temp            DS      1
temp1           DS      1
temp2           DS      1
pa              DS      1

nb_clocks       DS      1

prediv_ref0     DS      1
prediv_ref1     DS      1

_A              DS      1
Mmsb            DS      1
Mlsb            DS      1

_Arx            DS      1
Mlsbrx          DS      1
Mmsbrx          DS      1

_Atx            DS      1
Mlsbtx          DS      1
Mmsbtx          DS      1

;A              equ   RA.0  ;broche 17 du pic
;B              equ   RA.1  ;broche 18 du pic
;C              equ   RA.2  ;broche  1 du pic
;D              equ   RA.3  ;broche  2 du pic

ptt             equ   RA.4  ;broche 3 du pic (1=rx 0=tx)

lock_detect     equ   RB.4  ;broche 10 du pic (0=lock ok)
cmd_em	        equ   RB.3  ;broche 9 du pic (1=tx 0=rx invers� par transistor)

;I/O pour le PLL NJ8822
clock            =    RB.0  ;sortie clock,  broche 6 du pic.
data             =    RB.1  ;sortie datas,  broche 7 du pic.
enable           =    RB.2  ;sortie enable, broche 8 du pic.

; d�but du programme principal

                org    0
Start

		;movlw	7	;pour 16F627 / 16F628
		;movwf	1Fh	;pour 16F627 / 16F628 blocage du comparateur analogique

                mov    !RA,#00011111b  ; RA0 � RA4 = entr�es
                mov    !RB,#00010000b  ; RB0 � RB7 = sorties. (RB4 entr�e)
                mov    RB,#000h        ; les sorties � 0 au d�but.
                mov    count0,#0
                mov    count1,#0
                mov    prediv_ref0,#00000000b  ; 256 pour pas de 12.5Khz, lsb
                mov    prediv_ref1,#00100000b  ; suite msb osc r�f = 12.8Mhz/2
                                               ; mot de 3 bits, 5 lsb ignor�s.
;----------------------------------------------------------------------------
;lecture des entr�es RA0 , RA1 , RA2 , RA3
;                     1     2     4     8 => valeur binaire => canal
;renvoie sur les valeurs Mlsb??, Mmsb?? et _A?? du canal correspondant



;initialisation de la fr�quence
;------------------------------

:initfreq	mov	f,RA
		comf	f,0		;inversion (cavalier � la masse = 1), r�sultat dans w
		mov	temp1,w
                and   temp1,#00001111b

		cje   temp1,#0, :p0
		cje   temp1,#1, :p0x
		cje   temp1,#2, :p1
		cje   temp1,#3, :p1x
		cje   temp1,#4, :p2
		cje   temp1,#5, :p2x
		cje   temp1,#6, :p3
		cje   temp1,#7, :p3x

		cje   temp1,#8, :p4
		cje   temp1,#9, :p4x
		cje   temp1,#10, :p5
		cje   temp1,#11, :p5x
		cje   temp1,#12, :p6
		cje   temp1,#13, :p6x
		cje   temp1,#14, :pISS
		cje   temp1,#15, :pADR56

                jmp   :apres


;---------------------------------------------------------------------------
; caract�ristiques des canaux
; exemple pour le canal 0 => A,B,C,D tir�s au Vcc :

; 144,800 Tx:
; 144800 / 12,5 = 11584   et  11648 / 64 = 181,000
; 181 = 0B5 h soit Mmsbtx = 00h et Mlsbtx = 0B5h
; 0,000 x 64 = 00 soit _Atx = 00h


; 144,800 Rx:
; 166200 / 12,5 = 13296   et  13296 / 64 = 207,750
; 207 = 0CF h soit Mmsbrx = 00h et Mlsbrx = 0CFh
; 0,750 x 64 = 48 soit _Arx = 30h



;
:p0              mov   Mmsbtx,#000h		; 144,800 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#00h
                 mov   Mmsbrx,#000h		; 144,800 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#30h
                 jmp   :apres


:p0x             mov   Mmsbtx,#000h		; 144,8125 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#01h
                 mov   Mmsbrx,#000h		; 144,8125 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#31h
                 jmp   :apres


:p1              mov   Mmsbtx,#000h		; 144,825 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#02h
                 mov   Mmsbrx,#000h		; 144,825 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#32h
                 jmp   :apres


:p1x             mov   Mmsbtx,#000h		; 144,8375 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#03h
                 mov   Mmsbrx,#000h		; 144,8375 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#33h
                 jmp   :apres

:p2              mov   Mmsbtx,#000h		; 144,850 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#04h
                 mov   Mmsbrx,#000h		; 144,850 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#34h
                 jmp   :apres

:p2x             mov   Mmsbtx,#000h		; 144,8625 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#05h
                 mov   Mmsbrx,#000h		; 144,8625 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#35h
                 jmp   :apres

:p3              mov   Mmsbtx,#000h		; 144,875 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#06h
                 mov   Mmsbrx,#000h		; 144,875 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#36h
                 jmp   :apres

:p3x             mov   Mmsbtx,#000h		; 144,8875 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#07h
                 mov   Mmsbrx,#000h		; 144,8875 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#37h
                 jmp   :apres

:p4              mov   Mmsbtx,#000h		; 144,900 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#08h
                 mov   Mmsbrx,#000h		; 145,900 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#38h
                 jmp   :apres


:p4x             mov   Mmsbtx,#000h		; 144,9125 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#09h
                 mov   Mmsbrx,#000h		; 144,9125 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#39h
                 jmp   :apres


:p5              mov   Mmsbtx,#000h		; 144,925 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#0Ah
                 mov   Mmsbrx,#000h		; 144,925 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#3Ah
                 jmp   :apres


:p5x             mov   Mmsbtx,#000h		; 144,9375 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#0Bh
                 mov   Mmsbrx,#000h		; 144,9375 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#3Bh
                 jmp   :apres

:p6              mov   Mmsbtx,#000h		; 144,950 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#0Ch
                 mov   Mmsbrx,#000h		; 144,950 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#3Ch
                 jmp   :apres

:p6x             mov   Mmsbtx,#000h		; 144,9625 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#0Dh
                 mov   Mmsbrx,#000h		; 144,9625 MHz
                 mov   Mlsbrx,#0CFh
                 mov   _Arx,#3Dh
                 jmp   :apres

;fr�quences sp�ciales
; 145,9875 Tx:
; 145987,5 / 12,5 = 11679   et  11679 / 64 = 182,484375
; 182 = 0B6 h soit Mmsbtx = 00h et Mlsbtx = 0B6h
; 0,484375 x 64 = 31 soit _Atx = 1Fh

; 145,800 Rx:
; 167200 / 12,5 = 13376   et  13376 / 64 = 209,000
; 209 = 0D1 h soit Mmsbrx = 00h et Mlsbrx = 0D1h
; 0,000 x 64 = 00 soit _Arx = 00h


:pISS            mov   Mmsbtx,#000h		; 145,9875 MHz
                 mov   Mlsbtx,#0B6h
                 mov   _Atx,#1Fh
                 mov   Mmsbrx,#000h		; 145,800 MHz
                 mov   Mlsbrx,#0D1h
                 mov   _Arx,#00h
                 jmp   :apres

; 145,275 Tx:
; 145275 / 12,5 = 11622   et  11622 / 64 = 181,59375
; 181 = 0B5 h soit Mmsbtx = 00h et Mlsbtx = 0B5h
; 0,59375 x 64 = 38 soit _Atx = 26h

; 145,275 Rx:
; 166675 / 12,5 = 13334   et  13334 / 64 = 208,34375
; 208 = 0D0 h soit Mmsbrx = 00h et Mlsbrx = 0D0h
; 0,34375 x 64 = 22 soit _Arx = 16h


:pADR56          mov   Mmsbtx,#000h		; 145,275 MHz
                 mov   Mlsbtx,#0B5h
                 mov   _Atx,#06h
                 mov   Mmsbrx,#000h		; 145,275 MHz
                 mov   Mlsbrx,#0D0h
                 mov   _Arx,#16h
                 jmp   :apres



;----------------------------------------------------------------------------

:apres		call   tempo_lente
               	call   tempo_lente

;r�ception
;---------

:rx		clrb   cmd_em
    	        mov    Mmsb,Mmsbrx
		mov    Mlsb,Mlsbrx
		mov    _A,_Arx
               	call   envoi_vers_pll_div_princ
               	call   envoi_vers_pll_ref
		mov    Mmsb,Mmsbtx   		; apr�s envoi des donn�es Rx, chargement des variables
		mov    Mlsb,Mlsbtx 		; pour le Tx afin de gagner un peu de temps....
		mov    _A,_Atx

:rx_loop        jnb	ptt, :tx		;attente du Ptt pour passage en Tx
		mov	f,RA			;lecture entr�es v�rification si changement de canal
		comf	f,0			;inversion r�sultat dans w
		mov	temp2,w
		and	temp2,#00001111b
                cje	temp1,temp2,:rx_loop	;comparaison, si pas de changement on boucle
		jmp	:initfreq		;changement => rechargement de la fr�quence



;passage en �mission
;-------------------

:tx             call   envoi_vers_pll_div_princ
                call   envoi_vers_pll_ref

:lock_tx        snb    lock_detect      ;si pas lock tx, attente passage du Trx en �mission.
                jmp    :lock_tx		;si lock_tx, cette instruction est saut�e et passage en Tx
                setb    cmd_em
:tx_loop        jb      ptt, :rx     	;si broche ptt=1 retour en rx
                jmp     :tx_loop




; fin programme principal
;---------------------------------------------------------------------------
;---------------------------------------------------------------------------
;envoi taux de division r�f�rence 3bits MSB (5bits) ignor�s puis 8bits
envoi_vers_pll_ref
                mov     nb_clocks,#3
                mov     temp,prediv_ref1  ;envoi 3bits msb pr�div r�f�rence
:rotationA      movb    data,temp.7
                nop
                setb    clock
                nop
                nop
                nop
                clrb    clock
                rl      temp
                djnz    nb_clocks,:rotationA

                mov 	nb_clocks,#8
                mov     temp,prediv_ref0  	;envoi 8bits lsb pr�div r�f�rence
:rotationB      movb    data,temp.7
                nop
                setb    clock
                nop
                nop
                nop
                clrb    clock
                rl 	temp
                djnz   	nb_clocks,:rotationB
                clrb    data
                nop
                clrb    clock
                nop
                clrb    enable
                ret
;-------------------------------------------------------------------------
;-------------------------------------------------------------------------
;envoi taux de division principale 10bits (2msb + 8lsb)
envoi_vers_pll_div_princ
                clrb    enable
                clrb    clock
                clrb    data
                nop
                mov 	nb_clocks,#7
                mov     temp,_A       		;envoi 7bits pr�division modulus
:rotationF      movb    data,temp.6
                nop
                setb    clock
                nop
                setb    enable
                nop
                nop
                clrb    clock
                rl 	temp
                djnz   	nb_clocks,:rotationF

                mov 	nb_clocks,#2
                mov     temp,Mmsb       	;envoi 2bits poids faible du msb
:rotationD      movb    data,temp.1     	;6 bits msb ignor�s
                nop
                setb    clock
                nop
                nop
                nop
                clrb    clock
                rl 	temp
                djnz   	nb_clocks,:rotationD

                mov 	nb_clocks,#8
                mov     temp,Mlsb    		;envoi 8bits lsb division principale
:rotationE      movb    data,temp.7
                nop
                setb    clock
                nop
                nop
                nop
                clrb    clock
                rl 	temp
                djnz   	nb_clocks,:rotationE

;                clrb    data
;                nop
;                setb    clock
;                nop
;                clrb    enable
;                clrb    clock
                 ret
;---------------------------------------------------------------------------
;---------------------------------------------------------------------------
tempo_lente
                 mov     Count1,#255
:lop2            mov     Count0,#255       	;tempo_lente
:Loop2           djnz    Count0,:Loop2
                 djnz    Count1,:Lop2
                 ret
;---------------------------------------------------------------------------
tempo
                 mov     Count1,#20
:lop3            mov     Count0,#60       	;tempo rapide
:Loop3           djnz    Count0,:Loop3
                 djnz    Count1,:Lop3
                 ret
;---------------------------------------------------------------------------
