10 CLS
15 REM *********************************************************************
16 REM *                                                                   *
20 REM * Logiciel de programmation pour synthétiseur de fréquence MC145159 *
25 REM * par  F5HII & F6CSX (C) Juin 1999                                  *
26 REM *                                                                   *
29 REM *********************************************************************
30 REM
40 DIM R(14), N(18)
50 R(0) = 1:  ENVOI = 0
60 DX = &H378
70 REM DX: adresse héxa du port parallŠle
80 REM INV =1 si l'interface inverse les données, sinon INV=0
90 INV=1
100 K=255*INV
110 N(0)= 1-INV
115 REM N(0) est le bit de controle
120 REM mise … 0 de toutes les sorties de l'interface
130 OUT DX,K
140 CLS
150 PRINT "LOGICIEL UNIVERSEL DE PROGRAMMATION DU MC 145159 "
160 PRINT
170 IF INV=1 THEN 210
180 IF INV = 0 THEN 220
190 PRINT"erreur inv = 0 ou 1"
200 END
210 PRINT"Logiciel configuré pour interface inversant les données":GOTO 230
220 PRINT"Logiciel configuré pour interface n'inversant pas les données"
230 PRINT
240 INPUT " frequence du quartz de référence en MHz :",FXTAL
250 IF (FXTAL < 1) OR (FXTAL > 20) THEN 240
260 PRINT
270 PRINT "Le pas du synthétiseur est fixé … 12,5 kHz par défaut"
280 FCOMP = 12.5
290 REM R est le diviseur de reference R=FXTAL/FCOMP"
300 R = 1000 * FXTAL / FCOMP
310 PRINT "La valeur du diviseur de la référence est R="; R
320 MEMR = R
330 IF R < 3 THEN 340 ELSE 360
340 PRINT "frequence de comparaison trop grande"
350 GOTO 280
360 IF R > 16383 THEN 370 ELSE 390
370 PRINT "frequence de comparaison trop petite"
380 GOTO 280
390 INPUT "facteur de division du prescaler P = 32, 40 , 64 ou 128 ";P
400 IF P = 40 THEN 440
410 IF P = 32 THEN 440
420 IF P = 64 THEN 440
425 IF P=128 THEN 440
430 GOTO 390
440 INPUT "fréquence … synthétiser FVCO EN kHz"; FVCO
450 REM M est le diviseur global entre FVCO ET FCOMP
460 M = FVCO / FCOMP
470 IF (M < 543) OR (M > 131071!) THEN 480 ELSE 500
480 PRINT "erreur de programmation"
490 GOTO 240
500 IF M < 65535! THEN 520 ELSE 440
510 GOTO 410
520 REM calcul des diviseurs A et N
530 N = INT(M / P)
540 A = M - N * P
550 R = MEMR
560 REM mémorisation de R
570 PRINT "FVCO = (FXTAL/R).(NP+A)"
580 PRINT "R=";R
590 PRINT"N=";N
600 PRINT "A=";A
610 PRINT "P=";P
620 IF INV=0 THEN 670
630 REM complémentation de R, N et A
640 R = 16383-R
650 N= 1023-N
660 A=127-A
670 REM convertion en binaire des diviseurs A,N et R
680 REM convertion de R
690 REM R(0) et R(15) sont prealablement definis
700 FOR X = 14 TO 1 STEP -1
710 R(X) = INT(R / 2 ^ (X - 1))
720 R = R - R(X) * (2 ^ (X - 1))
730 NEXT X
740 REM convertion de N
750 REM N(0),bit de controle, est prealablement defini
760 REM N,A et le bit de controle forment un mot de 18 bits
770 FOR X = 10 TO 1 STEP -1
780 N(X + 7) = INT(N / 2 ^ (X - 1))
790 N = N - N(X + 7) * (2 ^ (X - 1))
800 NEXT X
810 REM convertion de A
820 FOR X = 7 TO 1 STEP -1
830 N(X) = INT(A / 2 ^ (X - 1))
840 A = A - N(X) * (2 ^ (X - 1))
850 NEXT X
860 PRINT"Affichage des données en binaire MSB <-----LSB":PRINT
870 PRINT"Diviseur de référence R (14 bits)"
880 PRINT "R(X)";
890 FOR X = 14 TO 1 STEP -1
900 PRINT R(X);
910 NEXT X
920 PRINT"Diviseur programmable N (10 bits), A (7 bits), bit de controle:"
930 PRINT "N(X)";
940 FOR X = 17 TO 0 STEP -1
950 PRINT N(X);
960 NEXT X
970 REM donnés sur BIT 0
980 REM clok   sur BIT 1
990 REM enable sur BIT 2
1000 REM envoi des données vers le synthétiseur
1010 REM envoi de R et P
1020 FOR X = 14 TO 1 STEP -1
1030 OUT DX, R(X)+254*INV
1040 OUT DX, R(X) + 2 +250*INV
1050 OUT DX, R(X)+ 254*INV
1060 NEXT X
1070 OUT DX, K
1080 REM envoi de l'impulsion de transfert
1090 GOTO 1140
1100 FOR X = 1 TO 10
1110 OUT DX, 4
1120 NEXT X
1130 OUT DX, K
1140 REM envoi de A et de N  A:7bits N:10bits
1150 FOR X = 17 TO 0 STEP -1
1160 OUT DX, N(X)+ 254*INV
1170 OUT DX, N(X) + 2 +250*INV
1180 OUT DX, N(X)+ 254*INV
1190 NEXT X
1200 OUT DX, K
1210 REM envoi de l'impulsion de transfert
1220 FOR X = 1 TO 10
1230 OUT DX, 4 + 247*INV
1240 NEXT X
1250 OUT DX, K
1260 PRINT
1270 PRINT " Taper F pour une nouvelle frequence, T pour terminer, R pour recommencer"
1280 A$ = INKEY$
1290 IF A$ = "f" OR A$="F" THEN 1330
1300 IF A$ = "t" OR A$ = "T" THEN 1350
1310 IF A$ = "r" OR A$ = "R" THEN 50
1320 GOTO 1280
1330 CLS
1340 GOTO 440
1350 END

